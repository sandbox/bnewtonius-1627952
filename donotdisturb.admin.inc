<?php

/**
 * @file
 * Admin include for the donotdisturb module 
 *
 * Defines all admin forms for this module as well as any submit or validate 
 * routines
 */


/**
 * Form constructor for the module's admin screens.
 *
 * @see user_login_form_validate()
 * @see user_login_form_submit()
 * @ingroup forms
 */

function donotdisturb_admin_settings() {
	$defPaths = array(
		'user/login',
		'logout',
		'user',
		'swfupload',
	);

	$form['exclue_paths'] = array(
		'#type' => 'textarea',
		'#title' => t('Exlude Paths'),
		'#default_value' => variable_get('exclude_paths', $defPaths),
		'#required' => TRUE, 
	);

	return system_settings_form($form);
}
